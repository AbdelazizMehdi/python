﻿
Function New-CustomChannel
{
<#
 .SYNOPSIS 
 Gaat ervoor zorgen om een kanaal aan te maken in MicrosftTeams(OS-Scripting) met een unieke ID op basis van een csv file

.NOTES
  Version:        5.1.18362.752
  Author:         Abdelaziz Mehdi
  Creation Date:  01/06/2020
  Purpose/Change: Examens OS-Scripting
  
 #>

 param
 (
 
[Parameter(Mandatory=$True,ValueFromPipeline=$true)]
[PSCustomObject]$myOBJ


 )

 begin 
 { 
 # beginstuk van mijn code om in te loggen 

 Import-Module MicrosoftTeams
 Connect-MicrosoftTeams -Credential $(Get-Credential)
 
 }
 process #achterliggende loop
 {

 try {
 
    New-TeamChannel -GroupId $myOBJ.ChannelID -DisplayName $myOBJ.Channel
 
    #probeer hier mijn code en van zodra je een error krijgt geef dit door aan de catch
 
    }
 catch
 { 
  #log error als er iets fout gaat in mijn code
 } 
 }
 end 
 {
 }
}