﻿Import-Module MicrosoftTeams
$kanaalNaam = Import-Csv -Path .\channels.csv #importeer csv file vanuit mijn path waar de file zich bevind
$cred = Get-Credential # maak variabel voor credentials in te geven (mijn s-nummer + password)
Connect-MicrosoftTeams -Credential $cred # verbind met MS teams en roep mijn credential-variabel op
foreach ($lijn in $kanaalNaam) # met een foreach loop lees ik deze lijn en maak ik een kanaal aan
{
New-TeamChannel -GroupId "fd8ebd00-3ebb-492a-b9c1-5b2ab6fc1736" -DisplayName $lijn.DisplayNaam -Description $lijn.Omschrijving 
}