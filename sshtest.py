import subprocess # importeer py bibiliotheek subprocess
diceIP = [] 
splitted = []
#lees elke lijn van mijn txt file en voeg mijn commandos toe aan mijn array diceIP
with open("hosts_and_ports.txt") as ip_port:
    for lijn in ip_port.readlines(): # in lijn zit 192.168.216.128:22\n
        removeNewline = lijn.strip() #strip() py functie haalt \n weg
        diceIP.append(removeNewline)
# ip wil ik splitten op de : zodat het volledige adres in 2 stukken word gekapt om nadien ipadres en poort apart aan te spreken
for ip in diceIP:
    splitted.append(ip.split(":")) 
# één stuk is mijn ipv4 addr en andere het poortaddr
for ipenport in splitted: # [[192.168.216.128,22][192.168.216.129,22][192.168.216.130,22]]
    ipv4adres = str(ipenport[0])
    portadres = str(ipenport[1])
 
 # ik maak een connectie via ssh en ik roep mijn txt file aan die een dir&file zal aanmaken
    with open('commando-ubuntu.txt') as fh: #leest de txt file als variabel fh
        sshrun =  subprocess.run("ssh.exe amehdi@"+ipv4adres +" -p"+ portadres, capture_output=True, text=True, shell=True, stdin=fh)
        #output = sshrun.stdout
        #foutmelding = sshrun.stderr
        print(sshrun.stdout)
        print(sshrun.stderr)
        # ssh capture output = als er een error is output het op scherm (capture_output) en slaag dit op in sshrun
        # standaard input = text file commando-ubuntu.txt