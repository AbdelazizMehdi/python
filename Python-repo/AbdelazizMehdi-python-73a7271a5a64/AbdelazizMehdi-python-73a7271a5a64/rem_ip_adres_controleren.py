def bestaat_uit_bytes(ipv4):
    from python_zelf_functies_schrijven import is_byte
    SplitIPAdress = ipv4.split(".")
    validIP = None
    for octet in SplitIPAdress:
        res = is_byte(int(octet))
        if res == False:
            validIP = False
            break
        else:
            validIP = True
    return validIP

print(bestaat_uit_bytes("267.128.101.67"))
