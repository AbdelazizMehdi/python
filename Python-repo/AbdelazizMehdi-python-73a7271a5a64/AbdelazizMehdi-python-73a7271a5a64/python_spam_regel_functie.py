def line_is_spam(lijn):
    zin = lijn.split(" ")
    y_spam = False
    for word in zin:
        if word == "lottery" or word == "inheritance" or word == "viagra":
            y_spam = True
    return y_spam