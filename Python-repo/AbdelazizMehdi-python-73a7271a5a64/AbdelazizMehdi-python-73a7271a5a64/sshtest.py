import subprocess # importeer bibiliotheek subprocess om een proces te runnen via python
diceIP = [] 
splitted = []
with open("hosts_and_ports.txt") as ip_port:
    for lijn in ip_port.readlines():
            # in lijn zit 192.168.216.128:22\n
        temp = lijn.strip() #strip functie haalt \n weg
        diceIP.append(temp)
# 2 for lussen aangemaakt eentje voor te splitten en andere de stukken
for ip in diceIP:
    splitted.append(ip.split(":"))
#ip wil je splitten op de : zodat je het in 2 stukken kan kappen, een stuk het ipv4 en andere het poort
for ipenport in splitted:
    ipv4adres = str(ipenport[0])
    portadres = str(ipenport[1]) # je kan geen getallen (int) meegeven bij subprocess.run enkel string (text) als je dit gaat concat (+ -p)
 
 # je maakt een connectie via ssh en je roept u txt file aan
    with open('commando-ubuntu.txt') as fh: #je leest het de txt file als een variabel
        sshrun =  subprocess.run("ssh.exe amehdi@"+ipv4adres +" -p"+ portadres, capture_output=True, text=True, shell=True, stdin=fh)
        # je roept ssh aan via ssh capture output = als er een error is output het op scherm (capture_output)
        # standaard input = u text file