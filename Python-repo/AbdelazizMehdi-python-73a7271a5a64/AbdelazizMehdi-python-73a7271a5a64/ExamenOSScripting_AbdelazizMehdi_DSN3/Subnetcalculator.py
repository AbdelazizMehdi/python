def ask_for_number_sequence(message):
    # prompt bericht aan de gebruiker wat is het ip adres en wat is het subnet masker?
    user_input = input(message + "\n")
    # 2 variabelen aangemaakt, eentje voor het ip adres te splitte en eentje voor naar een integer om te zetten 
    user_input_array_str = user_input.split(sep=".") # hier krijg ik een lijst terug met als voorbeeld ["192","168","13","4"]
    user_input_array_int = [int(elem) for elem in user_input_array_str] # hier zet ik elke octet om naar bv [192,168,13,4]
    # ik gebruik int() functie voor elk element in de array user_input_array_str om te zetten naar getallen ipv strings
    return user_input_array_int


def is_valid_ip_address(numberlist): #IP controleren
    # Check conditie 1: checkt of elk getal tussen 0 en 255 ligt
    # Check conditie 2: geef enkel True als ELKE octet in orde is klopt en false als het niet in orde is dus niet klopt. 
    # Van zodra er één octet niet in orde is spring uit de loop en return ongeldig
  for octet in numberlist:
        # check of kleiner is dan of gelijk aan 255 elem moet in range [0 - 255] zitten
        # kleiner_dan_of_gelijk_255 = elem <= 255
        # check of het groter_dan_of_gelijk_is_aan_0 = elem >= 0
        # check_of_aantal_elementen_exact 4 is = len(numberlist) == 4
        # Return True enkel en alleen als al mijn bovenstaande condities in orde zijn
    if octet <= 255 and octet >= 0 and len(numberlist) == 4: 
        is_Valid = True
    else:
        is_Valid = False
        break
    return is_Valid

def is_valid_netmask(numberlist): #Netmask controleren
    checking_ones = True # ik ga er hier vanuit dat het eerste binair getal een 1 is, onthou dit
    binary_netmask = "" # lege variabele om te initialiseren en hergebruiken in mijn loop
    # word in 4 stukken gekapt en elk gedeelte word dan overgezet naar binair in variabele binary_netmask
    if len(numberlist) == 4:
        for octet in numberlist:
            
            #elk octet binair uitrekenen (8 binaire getallen per octet) en wegschrijven naar mijn variabel binary netmask
            binary_netmask =  binary_netmask + f"{int(octet):08b}"
            
        for bit in binary_netmask:
            if checking_ones ==  True and bit == "1":
                isGeldig =  True
            elif checking_ones == True and bit == "0": # geldig tot je een 0 tegenkomt 
                checking_ones = False
                isGeldig =  True
            elif checking_ones == False and bit == "1":
                isGeldig =  False # als hem hier komt dan is het netmasker al ongeldig
    else:
        isGeldig = False  # als je bijvoorbeeld enkel 3 of 5 octets ingeeft dan stopt hij direct en checkt hij bovenstaande condities zelfs niet
    return isGeldig



def one_bits_in_netmask(netmasklist): # Functie om het aantal 1-bits te tellen in netmasker
    binary_netmask = "" # lege variabel om te hergebruiken in mijn loop
    counter = 0 # mijn teller initialiseren op 0
    for octet in netmasklist:
            #elk octet binair uitrekenen (8 binaire getallen per octet) en wegschrijven naar mijn variabel binary netmask
            binary_netmask =  binary_netmask + f"{int(octet):08b}" # binair omzetten van decimaal naar binair
    for bit in binary_netmask:
        if bit == "1": # van zodra je een 1 tegenkomt verhoog je de counter met 1
            counter += 1 # hij negeert alle nullen en telt enkel de 1'tjes op
    return counter # hier krijg je als eindresultaat CIDR notatie terug van /24 (allemaal 1tjes zonder de /)

def apply_network_mask(host_address, netmask): #network id berekenen, AND operatie 1 & 1 word 1 0 & 0 blijft 0 , 1 en 0 word 0
    subnetID = [] # ik begin hier met een lege lijst
    for index in range(0,4,1): # voorbeeld [192,168,32,2] [255,255,255,0] binair omgezet
        subnetID.append(int(host_address[index]) & int(netmask[index])) # hier vegelijk ik elk octet van ip met elk octet van netmask in binaire vorm
            
    return subnetID # resultaat = 192,168,32,0 


def netmask_to_wilcard_mask(subnetmasker): # functie gaat mijn wildcardmasker berekenen
    wildcardmasker = [] # ik maak een lege array om mijn wildcardmasker te berekenen en de output erin te steken
    binary_netmask = [] # ik maak een lege array voor dit te converteren naar binair
    
    for octet in subnetmasker: #[255,255,255,0] #hier loop ik 4 keer
        binary_netmask =  f"{int(octet):08b}" # read-able -> maakt het handig om te lezen
        byteconversion = "" # lege string
        for bit in binary_netmask: #hier loop ik 8 keer (32 keer in totaal)
            
            if bit == "1":
                byteconversion = byteconversion + "0" # hij voegt bij mijn variabele een 0 toe 
                
            elif bit == "0":
                byteconversion = byteconversion + "1" # van zodra je een 0 tegenkomt, plaats in byteconversation een 1
                
       
        wildcardmasker.append(int(byteconversion,2)) # byteconversation van binair naar decimaal overzetten (integer)
         
    return wildcardmasker # geef de waarde van wildcardmasker terug [0,0,0,255] als voorbeeld

def get_broadcast_address(network_address,wildcard_mask): # OR: 1 en 1 word 1 0 en 0 blijft 0 , 1 en 0 word 1
    broadcastAdres = [] # broadcast adres berekenen door OR operatie uit te voeren
    for index in range(0,4,1): # [192,168,32,0] [0,0,0,255]
        broadcastAdres.append(int(network_address[index]) | int(wildcard_mask[index])) 
        #hier vegelijk ik elk octet van netid met elk octet van wildcardmasker in binair VIA | CHECK
    
    return broadcastAdres

def prefix_length_to_max_hosts(lengthSubnet): 
    hostLengthSubnet = 32 - lengthSubnet  #er zijn dus 8 host bits gereserveerd voor het voorbeeld ip wat een klasse C is
    max_Hosts = (pow(2,hostLengthSubnet) -2) # (.0)netwerkid en broadcastadres(.255) ben je kwijt daarom heb ik hier -2 gedaan
    
    return max_Hosts # aantal interfaces dat je in u host kan krijgen of aantal hosts in het netwerk

if __name__ == "__main__": 
    ip = ask_for_number_sequence("Wat is het IP-adres?")
    subnetMask = ask_for_number_sequence("Wat is het SubnetMasker?")
    check_ipv4 = is_valid_ip_address(ip)
    check_Mask = is_valid_netmask(subnetMask)

    if check_ipv4 == True and check_Mask == True:
        print("IP-adres en subnetmasker zijn geldig.")
        ones = one_bits_in_netmask(subnetMask)
        print("De lengte van het subnetmasker is " + str(ones)) # ik kon hier geen strings met integer concateneren, daarom steeds omgezet naar str 

        netid = apply_network_mask(ip, subnetMask)
        print("Het adres van het subnet is " + str(netid))

        wildcardMasker = netmask_to_wilcard_mask(subnetMask)
        print("Het wildcardmasker is " + str(wildcardMasker))

        broadcastAddr = get_broadcast_address(ip,wildcardMasker)
        print("Het broadcastadres is " + str(broadcastAddr))

        maxAdressen = prefix_length_to_max_hosts(ones)
        print("Het maximaal aantal hosts op dit subnet is " + str(maxAdressen))
    else:
        print("IP-adres en/of subnetmasker is ongeldig. ")
        SystemExit() # als het een foutieve ip/masker is dus een ongeldige invoer dan stopt het programma