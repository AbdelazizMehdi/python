number_of_host_addresses = 4
number_of_host_bits = 1
available_host_addresses = 0

while available_host_addresses < number_of_host_addresses and number_of_host_bits < 33:
    number_of_host_bits += 1
    available_host_addresses = (available_host_addresses + 2) * 2 - 2

    print (available_host_addresses)
