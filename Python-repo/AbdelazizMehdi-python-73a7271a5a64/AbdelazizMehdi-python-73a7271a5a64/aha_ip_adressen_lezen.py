def read_ip(line):
    newline = []
    SplitIPAdress = line.split(".")
    for IP in SplitIPAdress:
        newline.append(int(IP))
    return newline

def read_ips():
    lijst = []
    with open('ips.txt') as fh:
        for line in fh.readlines():
            lijst.append(read_ip(line))
    return lijst