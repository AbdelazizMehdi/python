# aantal hosts dat je zoekt
number_of_host_addresses = 11
# minimum bits om zeker te zijn dat je genoeg ip adressen hebt
host_bits = 2
# 2 tot de macht van host bits (-2 broadcast & netwerkid) 
# 2 bits kunnen wij in totaal 9 ip adressen genereren, van die 9 ip adressen moeten we er 2 afgeven voor u netwerkid en broadcastid
while (pow(2,host_bits) -2) < number_of_host_addresses:
# hostbits +1 (we hebben nog een extra bit nodig)
    host_bits = host_bits +1
# if bits are more than 32 (not possible)    
    if host_bits >=32:
        print("veel teveel hosts")
#loop out of the script        
        break
# show aantal bits dat we nodig hebben om 50 ipadressen     
print(host_bits)