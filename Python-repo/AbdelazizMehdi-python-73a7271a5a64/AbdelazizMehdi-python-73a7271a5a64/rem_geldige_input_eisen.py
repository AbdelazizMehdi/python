def vraag_ja_of_neen():
    antwoord = None
    while not (antwoord == "ja" or antwoord == "neen"):
        antwoord = input("Gelieve ja of neen te antwoorden.\n")
    print("Bedankt voor je input!")
